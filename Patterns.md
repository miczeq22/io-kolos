## 1) Podział wzorców projektowych
Istnieją dwie grupy wzorców projektowych i ich podział określony jest według rodzaju wzorca i tego, co on robi
 (jest to pierwsza grupa wzorców projektowych) i według zakresu (czy dotyczy klas czy obiektów,
 jest to druga grupa).
Podział pierwszej grupy (według tego, co dany wzorzec robi i jakiego jest rodzaju) jest następujący:
 - `kreacyjne` - opisują one proces tworzenia obiektów. Ich celem jest tworzenie, inicjalizacja oraz konfiguracja obiektów, klas oraz innych typów danych.
 - `strukturalne` - opisujące struktury powiązanych ze sobą obiektów
 - `czynnościowe (behawioralne, operacyjne)` - opisujące zachowanie i odpowiedzialnośd współpracujących ze sobą obiektów.

Podział drugiej grupy, według tego, czy dotyczy klas lub obiektów:
 - `klasowe` - opisujące statyczne związki pomiędzy klasami
 - `obiektowy` - opisujące dynamiczne związki pomiędzy obiektami

Przykłady czynnościowych – Obserwator, Odwiedzający 
Przykłady strukturalnych – Adapter, Fasada

## 2) Wzorzec Adapter

Inaczej znany jako wrapper - jest to wzorzec klasowy lub obiektowy-strukturalny (można zaimplementowad go na
 dwa sposoby).
 Przeznaczenie: przekształca interfejs klasy na inny, oczekiwany przez klienta. Adapter umożliwia współdziałanie
 klasom, które z uwagi na niezgodny interfejs standardowo nie mogą współpracowad ze sobą.
Uzasadnienie: Czasem nie można powtórnie wykorzystad zaprojektowanej do wielokrotnego użytku klasy,
ponieważ jej interfejs nie jest zgodny ze specyficznym dla danej dziedziny interfejsem wymaganym przez
aplikacje. Adapter tłumaczy wywołania metod jego interfejsu na wywołania oryginalnego (docelowego)
interfejsu. Wzorzec adaptera stosowany jest najczęściej w przypadku, gdy wykorzystanie istniejącej klasy jest
niemożliwe ze względu na jej niekompatybilny interfejs. Drugim powodem może byd chęd stworzenia klasy, która
będzie współpracowała z klasami o nieokreślonych interfejsach.

Warunki zastosowania:
* Jeżeli zależy nam na wykorzystaniu istniejącej klasy, ale jej interfejs nie pasuje do tego, który jest
potrzebny
* Kiedy zależy nam na utworzeniu klasy do wielokrotnego użytku, współdziałającej z niepowiązanymi lub
niezależnymi klasami (czyli takimi, które niekoniecznie będą miały interfejsy zgodne z rozwijana klasa)
* Jeżeli trzeba użyd kilku istniejących podklas, ale dostosowywanie ich interfejsów przez utworzenie dla
każdej z nich następnej podklasy jest niepraktyczne. Adapter obiektowy może dostosować interfejs
swojej klasy nadrzędnej (dotyczy tylko adaptera obiektowego).

## 3) Wzorzec metoda wytwórcza

Jest to kreacyjny wzorzec projektowy, który tworzy obiekty.

* Aplikacja wykorzystująca metody wytwórcze jest niezależna od konkretnych implementacji zasobów
oraz procesu ich tworzenia. Mogą byd one ustalane dynamicznie w trakcie uruchomienia lub zmieniane
podczas działania aplikacji.

* Wzorzec hermetyzuje proces tworzenia obiektów, zamykając go za ściśle zdefiniowanym interfejsem.
Właściwośd ta jest wykorzystywana, gdy tworzenie nowego obiektu jest złożoną operacją (np. wymaga
wstrzyknięcia dodatkowych zależności).

* W wielu obiektowych językach programowania konstruktory klas muszą posiadad ściśle określone
nazwy, co może byd źródłem niejednoznaczności podczas korzystania z nich. Wzorzec umożliwia
zastosowanie nazwy opisowej oraz wprowadzenie kilku metod fabryki tworzących obiekt na różne
sposoby.

## 4) Wzorzec fasada

Jest to wzorzec strukturalny
Przeznaczenie: udostępnia jednolity interfejs dla zbioru interfejsów podsystemu. Fasada określa interfejs wyższego poziomu ułatwiający korzystanie z podsystemów.
Uzasadnienie:
Celem projektowym jest zminimalizowanie komunikacji i zależności miedzy podsystemami systemu złożonego. Jednym ze sposobów jest wprowadzenie obiektu fasadowego. Zasadniczo fasada stanowi otoczkę ujednolicającą różnorakie interfejsy wszystkich systemów składowych do postaci interfejsu prostszego albo bardziej dostępnego.

Warunki stosowania:
* Kiedy programista chce udostępnid prosty interfejs do złożonego podsystemu – zwiększa to możliwośd dostosowywania i powtórnego wykorzystania podsystemu
* Kiedy zależny nam na budowie systemu wieloplatformowego i ujednoliceniu/budowie spójnego interfejsu do różnych implementacji podsystemów
* Kiedy zależy nam na podzieleniu podsystemu na warstwy. Fasada śluzy wtedy do definiowania punktu wejścia do każdego poziomu podsystemu. Jeśli systemy sa zależne od siebie, można uprościd zależności miedzy nimi przez wykorzystanie do komunikacji tylko ich fasad
* Fasada dla systemu „w budowie” – kiedy system jest w budowie i nie określono jeszcze wystarczającego zestawu klas podsystemu. Fasada pozwala na korzystanie z elementów podsystemu w czasie, kiedy ten jest dopiero w budowie. Nawet jeśli ostatecznie będzie on zupełnie inny to przynajmniej pracownicy będą mogli prowadzid równolegle prace nad poszczególnymi elementami systemu.
* Fasada dla przeróbek – (tzw. Fasda faktoryzacji) jako środek oddzielający ‘dobry” kod od „złego” kodu, który ma byd przerobiony. Typowo stosowane w trakcie przerabiania i faktoryzacji systemu. Fasada stanowi tu tymczasowy interfejs, który udostępnia zarówno stare jak i nowe funkcje i elementy podsystemu

## 5) Wzorzec budowniczy

Jest to wzorzec kreacyjny-obiektowy.
Wzorzec budowniczy różni się od fabryki tym, że w fabryce tworzymy oddzielne obiekty. Budowniczy
używany jest wtedy, kiedy tworzymy obiekt, który składa się z kilku różnych obiektów. Ponadto fabryka
abstrakcyjna rożni się od Budowniczego, tym, że kładzie nacisk na tworzenie produktów z konkretnej rodziny, a
budowniczy kładzie nacisk na sposób tworzenia obiektów.

**Przeznaczenie:**
oddziela tworzenie złożonego obiektu od jego reprezentacji, dzięki czemu ten sam proces konstrukcji może prowadzid do powstania różnych reprezentacji. (Staramy się wyabstrahowad sposób tworzenia obiektu). We wzorcu tym nacisk jest położony na konstruowanie złożonych obiektów krok po kroku (etapami). Części musza byd tworzone w jakiejś kolejności lub przy użyciu określonego algorytmu.

**Warunki zastosowania:**
Jeżeli algorytm tworzenia obiektu złożonego powinien byd niezależny od składników tego obiektu i sposobu ich łączenia. Kiedy proces konstrukcji musi umożliwiad tworzenie różnych reprezentacji generowanego obiektu.

## 6) Wzorczec singleton

Przeznaczenie – gwarantuje, ze klasa będzie miała tylko jeden egzemplarz i zapewnia globalny dostęp do niego. W szczególnym przypadku wzorzec uogólnia się do wprowadzenia pewnej maksymalnej liczby obiektów, jakie mogą istnied w systemie [N-obiektów+.
Uzasadnienie – w przypadku w niektórych klas ważne jest, aby miały one jeden egzemplarz, np. menedżer plików (podsystem obsługi plików silnika gry), menedżer okien, bufor wydruku.
Struktura : definiuje się statyczna GetInstance umożliwiająca klientom dostęp do niepowtarzalnego egzemplarza klasy. Konstruktor kopiujący i operator przypisania powinno się schowad jako private.
Główna wada – nie gwarantuje pojedynczej instancji w srokowsku wielowątkowym (nie jest thread safe).

## 7) Wzorczec obserwator

Jest to wzorzec typu obiektowy – czynnościowy

 Skrótem: wzorzec definiuje zależnośd jeden-do-wielu pomiędzy obiektami tak, aby w momencie zmiany
 stanu jednego wszystkie zależne obiekty zostały poinformowane o zmianie stanu tego obiektu.

 Przeznaczenie: określa zależnośd jeden do wielu miedz obiektami. Kiedy zmieni się stan jednego z
 obiektów, wszystkie obiekty zależne od niego są o tym automatycznie powiadamiane i akutalizowane.
 

## 8) Wzorzec strategia

Strategia (ang. Strategy) jest czynnościowym wzorcem projektowym, będącym tak naprawdę pewną rodziną
algorytmów. Algorytmy te upakowane są w oddzielne, w pełni wymienne (w trakcie działania programu) klasy.
Na samej górze znajduje się klasa kontekstowa, wybierająca, który algorytm (ale tylko jeden!) będzie
wykonywany. Dodawanie nowych algorytmów sprowadza się do stworzenia nowej klasy implementującej dany
interfejs. Modyfikacja algorytmu to tylko modyfikacja odpowiedniej klasy. Mamy tutaj odseparowanie wyboru
algorytmu od jego implementacji (sam klient nawet nie wie jak zbudowana jest dana strategia).
Często występuje z wzorcem fabryki.

## 9) Wzorzec fabryka abstrakcyjna

kreacyjny-obiektowy

Przeznaczenie – udostępnia interfejs do tworzenia rodzin powiązanych ze sobą lub zależnych od siebie obiektów tego samego typu bez określania ich klas konkretnych.

Uzasadnienie – w przypadku niektórych implementacji istotne jest dostarczanie możliwości tworzenia różnych powiązanych ze sobą reprezentacji pod obiektów określając ich typ podczas działania programu. Fabryka abstrakcyjna rożni się od Budowniczego, tym, że kładzie nacisk na tworzenie produktów z konkretnej rodziny, a budowniczy kładzie nacisk na sposób tworzenia obiektów.

Warunki zastosowania:
* Kiedy system powinien byd niezależny od sposobu tworzenia, składania i reprezentowania produktowe,
* Jeśli system należy skonfigurowad za pomocą jednej z wielu rodzin produktów
* Jeżeli powiązane obiekty (produkty) jednej rodziny są zaprojektowane do wspólnego użytku i trzeba zapewnid
jednoczesne spójne korzystanie z tych produktów,
* Kiedy programista chce udostępnid klasę biblioteczna produktów i ujawniad jedynie ich interfejsy, a nie
implementacje

Przykład użycia – to sytuacja, gdy np. elementy graficzne aplikacji takie jak guziki, checkboxy powinny byd stworzone tylko raz przy wykorzystaniu jednej biblioteki graficznej (powiedzmy, ze Gtk), a w innym drugiej (np. Fltk). Kontrolki te stanowią pewna grupę, rodzinne obiektów i powinny być tworzone razem. Nie może być takiej sytuacji, ze przycisk w GUI programu jest z biblioteki Gtk, a checkbox z Fltk. 

Konsekwencje
* Izoluje klasy konkretne – pomaga kontrolowad klasy obiektów tworzony przez aplikacje
* Kapsułkuje zadanie i proces tworzenia obiektów-produktów izolując klientów od klas zawierających na
implementacje
* Ustawia zastępowanie rodzin produktów – łatwo jest zmienid fabrykę konkretna innym jej egzemplarzem bez
konieczności modyfikacji całego kodu
* Ułatwia zachowad spójnośd miedzy produktami poprzez zastosowanie interfejsu
* Utrudnia dodawanie obsługi produktów nowego rodzaju – rozszerzanie kolekcji fabryk konkretnych nie jest
zadaniem prostym (wymagane jest rozszerzenie interfejsu)

## 10) Wzorzec obserwator
[obiektowy - czynnościowy]
Przeznaczenie: Zadaniem Odwiedzającego jest odseparowanie algorytmu od struktury obiektowej na której operuje. Praktycznym rezultatem tego odseparowania jest możliwośd dodawania nowych operacji do aktualnych struktur obiektów bez konieczności ich modyfikacji.
Wzorzec umożliwia przejście po strukturze danych, oraz zebranie jakichś informacji. Gdy zaistnieje potrzeba zaimplementowania nowej funkcjonalności, gdzie pobieranie danych jest zaimplementowane tak samo, ale rodzaj danych będzie się różnid, problem ten rozwiąże się tworząc nową klasę „odwiedzającą” strukturę danych, która zbierze nowe informacje.