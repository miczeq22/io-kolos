# Inżynieria oprogramowania - wstęp

## Pojęcia `Inżynieria oprogramowania`
****

**Inżynieria oprogramowania** to dział informatyki, który zajmuje się wiedzą techniczną dotyczącą wszystkich faz cyklu życia oprogramowania. Traktuje oprogramowanie jako produkt, który ma spełniać potrzeby techniczne, ekonomiczne i społeczne

**zakres + czas + budżet => jakość**

## Cykl życia oprogramowania
****

Każdy projekt podlega podstawowym ograniczeniom:
- Budżet
- Zakres
- Czas

Cykl życia projektu można podzielić na cztery fazy
- `Faza konceptualizacji` - sporządzenie konspektu projektu.

- `Faza planowania` - etap definiowania i ustalania szczegółów ważnych elementów projektu.

- `Faza pracy` - faktyczne dostarczenie potrzebnych dóbr i usług, które przyczynią się do realizacji projektu i osiągnięcia celu.

- `Faza ewaluacji` - przed zakończeniem projektu lub w momencie jego zatrzymania następuje porównanie rzeczywiście wykonanego projektu z planami i założeniami. Jest to faza oceny projektu i rozwiązania zespołu projektowego.

**Analiza wymagań** - uzgodnienie wymagań klienta i ich analiza. Celem jest określenie zakresu prac, oszacowanie czasochłonności, kosztów i czasu wykonania projektu.