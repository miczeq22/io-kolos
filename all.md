## 1) Inżynieria oprogramowania
To dział informatyki, który zajmuje się wiedzą techniczną dotyczącą wszystkich faz cyklu życia oprogramowania. Traktuje oprogramowanie jako produkt, który ma spełniać potrzeby techniczne, ekonomiczne i społeczne
(zakres, czas, budżet => jakość)

## 2) Cykl życia oprogramowania 

Każdy projekt podlega podstawowym ograniczeniom:
* Budżet
* Zakres
* Czas

**Analiza wymagań** – uzgodnienie wymagań klienta i ich analiza. Celem jest określenie zakresu prac, oszacowanie czasochłonności, kosztów i czasu wykonania.

Cykl życia projektu można podzielić na cztery fazy:
* `Faza konceptualizacji` – sporządzenie konspektu projektu
* `Faza planowania` – etap definiowania i ustalania szczegółowo ważnych elementów projektu.
* `Faza pracy` – faktyczne dostarczenie potrzebnych dóbr i usług, które przyczynią się do realizacji projektu i osiągnięcia
* `Faza ewaluacji` – przed zakończeniem projektu lub w momencie jego zatrzymania następuje porównanie rzeczywiście wykonanego projektu z planami i założeniami, ocena projektu. Rozwiązanie zespołu projektowego.

## 3) Wymagania funkcjonalne i niefunkcjonalne
**Wymagania funkcjonalne**
Analiza wymagań funkcjonalnych umożliwia zidentyfikowanie i opisanie pożądanego zachowania systemu. Zgodnie z jedną z definicji, wymaganie funkcjonalne to „stwierdzenie, jakie usługi ma oferować system, jak ma reagować na określone dane wejściowe oraz jak ma się zachowywać w określonych sytuacjach. W niektórych wypadkach wymagania funkcjonalne określają, czego system nie powinien robić.

Jakie działania będzie mógł wykonać użytkownik ? Przykład: 
Użytkownik może się zalogować, wydrukować wynik, wyświetlić listę plików. 
Po wciśnięciu przycisku OK okno się zamyka i następuje powrót do okna głównego.

Jakie działanie wykonuje system ?
Przykład:
Cyklicznie (codziennie) wysyłana jest poczta do wskazanych użytkowników.

* dane wejściowe → działanie → dane wyjściowe
* ogólne – lista w języku naturalnym
* szczegółowa specyfikacja – dokładna lista, UML, grafy działania (zrobimy w późniejszej fazie projektowania)

Dobra dokumentacja wymagań nie powinna nadmiernie ograniczać projektu aplikacji, to znaczy narzucać konkretnego rozwiązania architektonicznego. Analityk powinien w taki sposób opisywać system, by prezentować dostępne funkcje i możliwości aplikacji bez zbędnego wnikania w szczegóły techniczne.

Podczas opisywania wymagań funkcjonalnych (zarówno listy wymagań, jak i szczegółowej specyfikacji), należy posługiwać się prostymi, jednoznacznymi i zrozumiałymi stwierdzeniami

 **Wymagania niefunkcjonalne**
 Ograniczenia w jakich system ma pracować, standardy jakie spełnia, itp.

Jaki ten system powinien być ?
* Co jest interfejsem ? (strona www, GUI, konsola, inne aplikacje)
* Czy jest przeznaczona dla pojedynczego użytkownika?
* Na jakim systemie operacyjnym działa?
* Jakie mam wymagania (dodatkowe oprogramowanie, dodatkowy sprzęt)?
    * wydajność
    * sklalowalność
    * otwartość, możliwość rozbudowy
    * odporność na awarie
    * bezpieczeństwo


## 3) Podział obowiązków w projekcie:
 - `Zamawiający` - odpowiada za zgodność systemu ze środowiskiem (w którym pracuje).
Jego zadaniem Jest zapewnienie realizacji przez system celów biznesowych.
Zainteresowany jest dobrym zdefiniowaniem i wspomaganiem usług dostarczanych przez zarządzane przez niego środowisko. Modele opisu środowiska powinny umożliwiać zamawiającemu możliwość oceny dostosowania systemu do potrzeb środowiska.

- `Analityk biznesowy` - prowadzi dialog pomiędzy światem biznesu I Informatyki.
Zajmuje się tworzeniem opisu środowiska poprzez formułowanie odpowiednich modeli.
Często projektuje również (nowe) rozwiązania dla funkcjonującego środowiska.
Tworzy modele umożliwiające opis struktury środowiska oraz Jego dynamiki (np. słownika pojęć)

- `Użytkownik` - będzie korzystał z systemu i jest zainteresowany zgodnością systemu ze środowiskiem oraz wygodą posługiwania się systemem.
Pełni podstawową funkcję w zakresie sprawdzania realizacji wymagań.
Modele czytane przez użytkownika powinny dobrze opisywać funkcjonalność oraz struktury danych występujących w systemie.

- `Analityk wymagań` - tworzy wymagania dla systemu oprogramowania.
Jego zadaniem jest zapewnienie zgodności wymagań ze środowiskiem oraz wygody użytkowania systemu. Potrzebuje modelu opisującego funkcjonalność systemu - model ten powinien dostarczać podziału na pojedyncze jednostki funkcjonalności.
(specjalizacje: projektanci interfejsu użytkownika).

- `Architekt` - Jest odpowiedzialny za zaprojektowanie całości systemu. Jest odpowiedzialny realizację systemu tzw.,nadzór architektoniczny" oraz dostosowanie architektury do aktualnych warunków realizacji systemu. Tworzone przez niego modele powinny zapewniać Jednoznaczne powiązanie z wymaganiami i jednocześnie zapewniać możliwość rozbudowy.

- `Projektant` - Wykonuje szczegółowy projekt podsystemów zgodnie z wytycznymi architekta.
Podstawą działań projektanta Jest tzw. „model architektoniczny" systemu.
Projektant opisuje sposób realizacji zdefiniowanych [tam] podsystemów.
Przedstawia strukturę, dynamikę oraz interakcje zachodzące w systemie oraz specyfikuje algorytmy. Tworzone przez niego modele są szczegółowym opisem poszczególnych składników kodu.

- `Programista` - Wykonuje system, pisząc kod odpowiednich podsystemów.
Zadaniem programisty jest zrealizowanie procedur i funkcji zadanych w projekcie.
W związku z tym zostaje zwolniony z definiowania struktury i specyfikowania algorytmów. (Często: projektant-programista)
Ważne jest rozdzielenie czynności projektowych od programistycznych!

- `Recezent` - Pełni rolę sprawdzającego jakość modeli tworzonych przez pozostałe role.
Inspekcja modeli przez niezależną osobę jest bardzo istotnym elementem zapewnienia jakości systemu oprogramowania.

- `Tester` - Sprawdza jakość kodu. Często poprzez porównanie działającego systemu z jego modelem na poziomie wymagań. W celu sprawdzenia wymagań tester projektuje i przeprowadza testy akceptacyjne systemu.

## 4) FURPS
Zbieranie wymagań jest pierwszym krokiem do właściwego testowania. Czynność ta jest wspierana przez metodę klasyfikacji FURPS. Angielski akronim rozszyfrowujemy następująco:

* `Functionality` funkcjonalność w rozumieniu zestawu funkcji uwzględniająca również bezpieczeństwo, możliwości systemu
* `Usability` użyteczność jako zestaw wizualnych aspektów oprogramowania, estetyka, dokumentacja
* `Reliability` niezawodność, będąca mierzona np. częstością występowania błędów
* `Performance` wydajność aplikacji określana również jako czas odpowiedzi lub użycie zasobów
* `Supportability` przenosność, rozszerzalność, nie dająca się łatwo przetłumaczyć “wspieralność” uwzględniająca zdolność aplikacji do instalacji na różnych platformach, łatwość testowania itd.

## 5) Diagram przejść przez stany STD
Diagramy STD opisują zachowanie systemu w czasie. Mogą również służyć do reprezentacji sposobu realizacji funkcji systemu.
Składają się z podstawowych składników:
* Zdarzenie (event) – informuje o tym, co wydarzyło się w danej chwili, ma zerowy czas trwania
* Stan (state) – okres czasu pomiędzy zdarzeniami w systemie. System w różnych stanach reaguje w sposób
jakościowo różny na zachodzące zdarzenia.
* Przejście (transition) – zmiana stanu wywołana przez zdarzenie, może być uwarunkowana spełnieniem dodatkowych
kryteriów.
* Akcja (action) – czynność wykonywana w momencie zajścia zdarzenia.
* Operacja (activity) – związana z konkretnym stanem, oznaczająca czynność ciągłą, wykonywaną podczas trwania stanu.
Operacja może zostać przerwana przez zdarzenia, które powoduje przejście do nowego stanu.

## 6) Diagram przypadków użycia

Diagram przypadków użycia należy do grupy diagramów zachowań.
Pokazuje on system z punktu widzenia użytkownika. Obrazuje, co robi system, a nie jak to robi. 
Najczęściej wraz z diagramami przypadków użycia konieczne jest dostarczenie dokumentacji.

Składa się z:
* `przypadek użycia` - diagram przypadków użycia należy do grupy diagramów zachowań.
Pokazuje on system z punktu widzenia użytkownika. Obrazuje, co robi system, a nie jak to robi. Najczęściej wraz z diagramami przypadków użycia konieczne jest dostarczenie dokumentacji.
![przypadek użycia](./img/1.png)

* `Aktor` - Ma unikalną nazwę. Aktorzy mogą być:
    * Ludźmi wchodzącymi w interakcję z systemem,
    * Systemami zewnętrznymi,
    * Częściami systemu, które mają wpływ na funkcjonowanie systemu, ale same przez ten system nie mogą być zmieniane.

![aktor](./img/2.png)

* `Interakcja` - Obrazuje interakcję pomiędzy przypadkiem użycia a aktorem. Odcinek łączący. (Symbol linii)

* `Blok ponownego użycia` - Obrazuje fragment systemu, który jest używany przez kilka przypadków użycia.
![ponowne użycie](./img/3.png)

* `Relacja include lub extended` -  Pokazuje związek zachodzący między dwoma przypadkami użycia lub przypadkiem użycia a blokiem ponownego użycia.
![relacja](./img/4.png)

* `Nazwa systemu wraz z otoczeniem systemu` - Obrazuje granicę pomiędzy systemem a jego otoczeniem.
![otoczenie](./img/5.png)
  

## 7) Diagram przepływy danych DFD

Diagramy przepływu danych (DFD) wykorzystuje się do modelowania pod kątem przekazywania danych między procesami i obiektami w procesie. DFD służy więc do prezentowania w jaki sposób dane są przetwarzane w systemie. DFD znajdują równie zastosowanie w modelowaniu procesów gospodarczych organizacji oraz w planowaniu gospodarczym i strategicznym.

Tworzenie DFD opiera się na czterech kategoriach pojęciowych:
* Procesów/funkcji – realizujących określone cele
* Przepływów – ilustrujących kierunek przesyłu danych w systemie
* Składnic/magazynów danych – będących argumentami dla procesów i funkcji
* Obiektów zewnętrznych/terminatorów – stanowiących odbiorców bądź źródła danych.

Dwie najczęściej stosowane notacje graficzne służące do opisu DFD: Yourdona-DeMarco i Ganea-Sarsona (np. w MS Visio):
![](./img/6.png)

## 8) Diagram wdrożenia/rozlokowania

Diagramów wdrożenia używa się, by opisać fizyczne aspekty struktury systemy. Główne cele używania diagramów wdrożenia:
* Wizualizacja fizycznej topologii systemu
* Opis komponentów hardwareowych, na których wdrażane są komponenty softwarowe.
![](./img/7.png)

## 9) Diagram komponentów

Diagram komponentów pozwala na modelowanie elementów oprogramowania i związków miedzy nimi. Typowe są dla niego interfejsy, które łączą komponenty.

![](./img/8.png)

## 10) Diagram maszyny stanowej

Maszyna stanowa – zachowanie określające sekwencje stanów (bliźniacze do STD), przez które przechodzi obiekt bądź reakcja w odpowiedzi na zdarzenie.

![](./img/9.png)

## 11) Diagram sekwencji
Diagram sekwencji prezentuje komunikowanie się obiektów w czasie. Składa się z obiektów, komunikatów i czasu.
![](./img/10.png)

## 12) Diagram klas
Diagram klas przedstawia klasy występujące w systemie i statyczne relacje pomiędzy nimi wraz z ograniczeniami. Jest podstawowym diagramem perspektywy logicznej systemu.

![](./img/11.png)
![](./img/12.png)

Z diagramem klas związany jest także **diagram obiektów**:
![](./img/13.png)
Diagram obiektów to wystąpienie diagramu klas, odwzorowujące strukturę systemu w wybranym momencie jego działania.


## 13) Diagram czynności
Diagram czynności przedstawia sekwencje i (lub) współbieżne przepływy sterowania oraz dane pomiędzy uporządkowanymi ciągami czynności, akcji i obiektów.
Stosuje się je w modelowaniu:
* Procesów
* Systemów oraz podsystemów
* Scenariuszy przypadków użycia
* Operacji
* Algorytmów

![](./img/14.png)
![](./img/15.png)

## 14) Model buduj i poprawiaj
Jest to model cyklu życia oprogramowania stosowany podczas produkcji na własny użytek, dla niewielkich problemów.

![](./img/16.png)

## 15) Model V
Jest to zmodyfikowany model kaskadowy, podkreślający wagę weryfikacji i walidacji systemu.

![](./img/17.png)
Każdy etap prowadzi do zwiększenia szczegółowości definicji aż do osiągnięcia dolnego punktu oznaczającego wytworzenie kodu modułów. Kolejne kroki to kroki integracyjne umieszczone na drugim ramieniu.

## 16) Model spiralny
![](./img/18.png)
Model spiralny został zaproponowany w 1988 roku jako ogólna koncepcja iteracyjnego rozwoju oprogramowania. Każde okrążenie po spirali reprezentuje planowanie i wytworzenie pewnego elementu produktu.
* Planowanie – ustalenie celów przygotowania elementu lub produkcji kolejnej wersji systemu, identyfikowanie alternatyw i ograniczeń.
* Analiza ryzyka – ocena alternatyw i oszacowanie ryzyka, ewentualnie jego minimalizacja (np. budowa prototypu).
* Konstrukcja – wytworzenie kolejnego przybliżenia produktu.
* Atestowanie – Walidacja przez klienta, jeśli nie jest pozytywna: rozpoczyna się kolejny proces.

Zalety: 
* Możliwość zmian kierunku rozwoju systemu pomiędzy „obrotami”
* Ścisła współpraca z klientem
* Uświadomienie konieczności zarządzania ryzykiem

Wady: 
* Przydatny dla systemów, które mogą być wdrażane przy okrojonej funkcjonalności i obniżonej jakości
* Osiągnięcie wersji finalnej może wymagać dużo czasu

## 17) Model kaskadowy
![](./img/19.png)
Używamy go, gdy wymagania są dobrze zdefiniowane lub projekt jest podobny do realizowanych wcześniej. Model kaskadowy jest liniowy, sekwencyjny, stanowi punkt wyjścia do budowy innych modeli.

Zalety:
* Łatwość zarządzania
* Ułatwia planowanie, monitorowanie przedsięwzięć
* Wymaga dobrze sprecyzowanych wymagań

Wady:
* Brak weryfikacji, walidacji, elastyczności
* Narzuca ścisłą kolejność wykonywania prac
* Powoduje wysokie koszty błędów popełnionych we wcześniejszych fazach
* Rzadki kontakt z klientem

## 18) Model Kruchtena 4 + 1
Jest jednym z praktycznych sposobów „rozbijania” diagramów UML kolejnych modeli.
![](./img/20.png)

## 19) Słownik danych
Nie da się modelować wymagań użytkownika bez zdefiniowanych danych występujących w systemie. Słownik danych (DD) stanowi składnicę wszystkich pojęć zdefiniowanych w systemie (projekcie). Jest zorganizowaną listą elementów systemu, zawierającą definicję tych elementów.
W skład słownika danych wchodzą opisy:
* Złożonych agregatów pakietów danych, składających się z komponentów (np. pół rekordów) – używanych przez przepływy danych (data flows).
* Złożonych agregatów danych w składach danych.
* Szczegółowych relacji pomiędzy obiektami diagramów ERD (encji-relacji).

Często używa się następujqcych symboli formalizmu notacji dla DD:
* `=` - składa się z (definicja kompozycji)
* `+` - złączenienie
* `()` - opcia
* `{}` - iteracja
* `[]` - wybranie jednej z kilku możliwych
* `**` - komentarz między znakami `*`
* `@` - identyfikator (pole klucz)
* `|` - oddziela alternatywne wybory w `[]`
```
Osoba = tytuł + imię + (drugie_imię) + nazwisko Tytuł = [ Pan | Pani | Mgr. | Dr | dr hab. | Profesor ] imię = { dowolny_znak }
```

```
drugie_imię = { dowolny_znak }
```

```
dowolny_znak = [A-Z | a-z | 0-9 ]
```

## 20) UML
Język modelowania UML to zestandaryzowany, formalny język modelowania składający się ze zintegrowanych zbiorów diagramów. Oparty jest na licencji public domain, obecnie rozwijany przez organizację OMG (Object Management Group).
UML stosowany jest do modelowania dziedziny problemu:
* Tworzenie specyfikacji,
* Wizualizacja
* Projektowanie architektury
* Wizualizacja procesów
* Tworzenie diagramów klas i obiektów
* Tworzenie dokumentacji.

UML jest językiem modelowania, a nie programowania. Ułatwia projektowanie systemu z wykorzystaniem metodyki obiektowej. Pozwala obrazować, specyfikować, tworzyć i dokumentować elementy systemu. Nie jest narzędziem ani metodyką.
Posiada dwie składowe: notację elementów oraz ich semantykę (metamodel).

**Notacja:**
 * Elementy graficzne
 * Składnia języka modelowania
 
 * Sposób zapisu modeli elementów

**Metamodel:**
 * Definicje składni i semantyki wprowadzonego modelu
 * Ustala elementy składni diagramów otoczenia typologiczne

## 21) Podział wzorców projektowych
Istnieją dwie grupy wzorców projektowych i ich podział określony jest według rodzaju wzorca i tego, co on robi
 (jest to pierwsza grupa wzorców projektowych) i według zakresu (czy dotyczy klas czy obiektów,
 jest to druga grupa).
Podział pierwszej grupy (według tego, co dany wzorzec robi i jakiego jest rodzaju) jest następujący:
 - `kreacyjne` - opisują one proces tworzenia obiektów. Ich celem jest tworzenie, inicjalizacja oraz konfiguracja obiektów, klas oraz innych typów danych.
 - `strukturalne` - opisujące struktury powiązanych ze sobą obiektów
 - `czynnościowe (behawioralne, operacyjne)` - opisujące zachowanie i odpowiedzialnośd współpracujących ze sobą obiektów.

Podział drugiej grupy, według tego, czy dotyczy klas lub obiektów:
 - `klasowe` - opisujące statyczne związki pomiędzy klasami
 - `obiektowy` - opisujące dynamiczne związki pomiędzy obiektami

Przykłady czynnościowych – Obserwator, Odwiedzający 
Przykłady strukturalnych – Adapter, Fasada

## 22) Wzorzec Adapter

Inaczej znany jako wrapper - jest to wzorzec klasowy lub obiektowy-strukturalny (można zaimplementowad go na
 dwa sposoby).
 Przeznaczenie: przekształca interfejs klasy na inny, oczekiwany przez klienta. Adapter umożliwia współdziałanie
 klasom, które z uwagi na niezgodny interfejs standardowo nie mogą współpracowad ze sobą.
Uzasadnienie: Czasem nie można powtórnie wykorzystad zaprojektowanej do wielokrotnego użytku klasy,
ponieważ jej interfejs nie jest zgodny ze specyficznym dla danej dziedziny interfejsem wymaganym przez
aplikacje. Adapter tłumaczy wywołania metod jego interfejsu na wywołania oryginalnego (docelowego)
interfejsu. Wzorzec adaptera stosowany jest najczęściej w przypadku, gdy wykorzystanie istniejącej klasy jest
niemożliwe ze względu na jej niekompatybilny interfejs. Drugim powodem może byd chęd stworzenia klasy, która
będzie współpracowała z klasami o nieokreślonych interfejsach.

Warunki zastosowania:
* Jeżeli zależy nam na wykorzystaniu istniejącej klasy, ale jej interfejs nie pasuje do tego, który jest
potrzebny
* Kiedy zależy nam na utworzeniu klasy do wielokrotnego użytku, współdziałającej z niepowiązanymi lub
niezależnymi klasami (czyli takimi, które niekoniecznie będą miały interfejsy zgodne z rozwijana klasa)
* Jeżeli trzeba użyd kilku istniejących podklas, ale dostosowywanie ich interfejsów przez utworzenie dla
każdej z nich następnej podklasy jest niepraktyczne. Adapter obiektowy może dostosować interfejs
swojej klasy nadrzędnej (dotyczy tylko adaptera obiektowego).

## 23) Wzorzec metoda wytwórcza

Jest to kreacyjny wzorzec projektowy, który tworzy obiekty.

* Aplikacja wykorzystująca metody wytwórcze jest niezależna od konkretnych implementacji zasobów
oraz procesu ich tworzenia. Mogą byd one ustalane dynamicznie w trakcie uruchomienia lub zmieniane
podczas działania aplikacji.

* Wzorzec hermetyzuje proces tworzenia obiektów, zamykając go za ściśle zdefiniowanym interfejsem.
Właściwośd ta jest wykorzystywana, gdy tworzenie nowego obiektu jest złożoną operacją (np. wymaga
wstrzyknięcia dodatkowych zależności).

* W wielu obiektowych językach programowania konstruktory klas muszą posiadad ściśle określone
nazwy, co może byd źródłem niejednoznaczności podczas korzystania z nich. Wzorzec umożliwia
zastosowanie nazwy opisowej oraz wprowadzenie kilku metod fabryki tworzących obiekt na różne
sposoby.

## 24) Wzorzec fasada

Jest to wzorzec strukturalny
Przeznaczenie: udostępnia jednolity interfejs dla zbioru interfejsów podsystemu. Fasada określa interfejs wyższego poziomu ułatwiający korzystanie z podsystemów.
Uzasadnienie:
Celem projektowym jest zminimalizowanie komunikacji i zależności miedzy podsystemami systemu złożonego. Jednym ze sposobów jest wprowadzenie obiektu fasadowego. Zasadniczo fasada stanowi otoczkę ujednolicającą różnorakie interfejsy wszystkich systemów składowych do postaci interfejsu prostszego albo bardziej dostępnego.

Warunki stosowania:
* Kiedy programista chce udostępnid prosty interfejs do złożonego podsystemu – zwiększa to możliwośd dostosowywania i powtórnego wykorzystania podsystemu
* Kiedy zależny nam na budowie systemu wieloplatformowego i ujednoliceniu/budowie spójnego interfejsu do różnych implementacji podsystemów
* Kiedy zależy nam na podzieleniu podsystemu na warstwy. Fasada śluzy wtedy do definiowania punktu wejścia do każdego poziomu podsystemu. Jeśli systemy sa zależne od siebie, można uprościd zależności miedzy nimi przez wykorzystanie do komunikacji tylko ich fasad
* Fasada dla systemu „w budowie” – kiedy system jest w budowie i nie określono jeszcze wystarczającego zestawu klas podsystemu. Fasada pozwala na korzystanie z elementów podsystemu w czasie, kiedy ten jest dopiero w budowie. Nawet jeśli ostatecznie będzie on zupełnie inny to przynajmniej pracownicy będą mogli prowadzid równolegle prace nad poszczególnymi elementami systemu.
* Fasada dla przeróbek – (tzw. Fasda faktoryzacji) jako środek oddzielający ‘dobry” kod od „złego” kodu, który ma byd przerobiony. Typowo stosowane w trakcie przerabiania i faktoryzacji systemu. Fasada stanowi tu tymczasowy interfejs, który udostępnia zarówno stare jak i nowe funkcje i elementy podsystemu

## 25) Wzorzec budowniczy

Jest to wzorzec kreacyjny-obiektowy.
Wzorzec budowniczy różni się od fabryki tym, że w fabryce tworzymy oddzielne obiekty. Budowniczy
używany jest wtedy, kiedy tworzymy obiekt, który składa się z kilku różnych obiektów. Ponadto fabryka
abstrakcyjna rożni się od Budowniczego, tym, że kładzie nacisk na tworzenie produktów z konkretnej rodziny, a
budowniczy kładzie nacisk na sposób tworzenia obiektów.

**Przeznaczenie:**
oddziela tworzenie złożonego obiektu od jego reprezentacji, dzięki czemu ten sam proces konstrukcji może prowadzid do powstania różnych reprezentacji. (Staramy się wyabstrahowad sposób tworzenia obiektu). We wzorcu tym nacisk jest położony na konstruowanie złożonych obiektów krok po kroku (etapami). Części musza byd tworzone w jakiejś kolejności lub przy użyciu określonego algorytmu.

**Warunki zastosowania:**
Jeżeli algorytm tworzenia obiektu złożonego powinien byd niezależny od składników tego obiektu i sposobu ich łączenia. Kiedy proces konstrukcji musi umożliwiad tworzenie różnych reprezentacji generowanego obiektu.

## 26) Wzorczec singleton

Przeznaczenie – gwarantuje, ze klasa będzie miała tylko jeden egzemplarz i zapewnia globalny dostęp do niego. W szczególnym przypadku wzorzec uogólnia się do wprowadzenia pewnej maksymalnej liczby obiektów, jakie mogą istnied w systemie [N-obiektów+.
Uzasadnienie – w przypadku w niektórych klas ważne jest, aby miały one jeden egzemplarz, np. menedżer plików (podsystem obsługi plików silnika gry), menedżer okien, bufor wydruku.
Struktura : definiuje się statyczna GetInstance umożliwiająca klientom dostęp do niepowtarzalnego egzemplarza klasy. Konstruktor kopiujący i operator przypisania powinno się schowad jako private.
Główna wada – nie gwarantuje pojedynczej instancji w srokowsku wielowątkowym (nie jest thread safe).

## 27) Wzorczec obserwator

Jest to wzorzec typu obiektowy – czynnościowy

 Skrótem: wzorzec definiuje zależnośd jeden-do-wielu pomiędzy obiektami tak, aby w momencie zmiany
 stanu jednego wszystkie zależne obiekty zostały poinformowane o zmianie stanu tego obiektu.

 Przeznaczenie: określa zależnośd jeden do wielu miedz obiektami. Kiedy zmieni się stan jednego z
 obiektów, wszystkie obiekty zależne od niego są o tym automatycznie powiadamiane i akutalizowane.
 

## 28) Wzorzec strategia

Strategia (ang. Strategy) jest czynnościowym wzorcem projektowym, będącym tak naprawdę pewną rodziną
algorytmów. Algorytmy te upakowane są w oddzielne, w pełni wymienne (w trakcie działania programu) klasy.
Na samej górze znajduje się klasa kontekstowa, wybierająca, który algorytm (ale tylko jeden!) będzie
wykonywany. Dodawanie nowych algorytmów sprowadza się do stworzenia nowej klasy implementującej dany
interfejs. Modyfikacja algorytmu to tylko modyfikacja odpowiedniej klasy. Mamy tutaj odseparowanie wyboru
algorytmu od jego implementacji (sam klient nawet nie wie jak zbudowana jest dana strategia).
Często występuje z wzorcem fabryki.

## 29) Wzorzec fabryka abstrakcyjna

kreacyjny-obiektowy

Przeznaczenie – udostępnia interfejs do tworzenia rodzin powiązanych ze sobą lub zależnych od siebie obiektów tego samego typu bez określania ich klas konkretnych.

Uzasadnienie – w przypadku niektórych implementacji istotne jest dostarczanie możliwości tworzenia różnych powiązanych ze sobą reprezentacji pod obiektów określając ich typ podczas działania programu. Fabryka abstrakcyjna rożni się od Budowniczego, tym, że kładzie nacisk na tworzenie produktów z konkretnej rodziny, a budowniczy kładzie nacisk na sposób tworzenia obiektów.

Warunki zastosowania:
* Kiedy system powinien byd niezależny od sposobu tworzenia, składania i reprezentowania produktowe,
* Jeśli system należy skonfigurowad za pomocą jednej z wielu rodzin produktów
* Jeżeli powiązane obiekty (produkty) jednej rodziny są zaprojektowane do wspólnego użytku i trzeba zapewnid
jednoczesne spójne korzystanie z tych produktów,
* Kiedy programista chce udostępnid klasę biblioteczna produktów i ujawniad jedynie ich interfejsy, a nie
implementacje

Przykład użycia – to sytuacja, gdy np. elementy graficzne aplikacji takie jak guziki, checkboxy powinny byd stworzone tylko raz przy wykorzystaniu jednej biblioteki graficznej (powiedzmy, ze Gtk), a w innym drugiej (np. Fltk). Kontrolki te stanowią pewna grupę, rodzinne obiektów i powinny być tworzone razem. Nie może być takiej sytuacji, ze przycisk w GUI programu jest z biblioteki Gtk, a checkbox z Fltk. 

Konsekwencje
* Izoluje klasy konkretne – pomaga kontrolowad klasy obiektów tworzony przez aplikacje
* Kapsułkuje zadanie i proces tworzenia obiektów-produktów izolując klientów od klas zawierających na
implementacje
* Ustawia zastępowanie rodzin produktów – łatwo jest zmienid fabrykę konkretna innym jej egzemplarzem bez
konieczności modyfikacji całego kodu
* Ułatwia zachowad spójnośd miedzy produktami poprzez zastosowanie interfejsu
* Utrudnia dodawanie obsługi produktów nowego rodzaju – rozszerzanie kolekcji fabryk konkretnych nie jest
zadaniem prostym (wymagane jest rozszerzenie interfejsu)

## 30) Wzorzec obserwator
[obiektowy - czynnościowy]
Przeznaczenie: Zadaniem Odwiedzającego jest odseparowanie algorytmu od struktury obiektowej na której operuje. Praktycznym rezultatem tego odseparowania jest możliwośd dodawania nowych operacji do aktualnych struktur obiektów bez konieczności ich modyfikacji.
Wzorzec umożliwia przejście po strukturze danych, oraz zebranie jakichś informacji. Gdy zaistnieje potrzeba zaimplementowania nowej funkcjonalności, gdzie pobieranie danych jest zaimplementowane tak samo, ale rodzaj danych będzie się różnid, problem ten rozwiąże się tworząc nową klasę „odwiedzającą” strukturę danych, która zbierze nowe informacje.