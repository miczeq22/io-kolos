## 1) Inżynieria oprogramowania
To dział informatyki, który zajmuje się wiedzą techniczną dotyczącą wszystkich faz cyklu życia oprogramowania. Traktuje oprogramowanie jako produkt, który ma spełniać potrzeby techniczne, ekonomiczne i społeczne
(zakres, czas, budżet => jakość)

## 2) Cykl życia oprogramowania 

Każdy projekt podlega podstawowym ograniczeniom:
* Budżet
* Zakres
* Czas

**Analiza wymagań** – uzgodnienie wymagań klienta i ich analiza. Celem jest określenie zakresu prac, oszacowanie czasochłonności, kosztów i czasu wykonania.

Cykl życia projektu można podzielić na cztery fazy:
* `Faza konceptualizacji` – sporządzenie konspektu projektu
* `Faza planowania` – etap definiowania i ustalania szczegółowo ważnych elementów projektu.
* `Faza pracy` – faktyczne dostarczenie potrzebnych dóbr i usług, które przyczynią się do realizacji projektu i osiągnięcia celu.
* `Faza ewaluacji` – przed zakończeniem projektu lub w momencie jego zatrzymania następuje porównanie rzeczywiście wykonanego projektu z planami i założeniami, ocena projektu.Rozwiązanie zespołu projektowego.

## 3) Wymagania funkcjonalne i niefunkcjonalne

**Wymagania funkcjonalne**
Analiza wymagań funkcjonalnych umożliwia zidentyfikowanie i opisanie pożądanego zachowania systemu. Zgodnie z jedną z definicji, wymaganie funkcjonalne to „stwierdzenie, jakie usługi ma oferować system, jak ma reagować na określone dane wejściowe oraz jak ma się zachowywać w określonych sytuacjach. W niektórych wypadkach wymagania funkcjonalne określają, czego system nie powinien robić.

Jakie działania będzie mógł wykonać użytkownik?
Przykład: 
Użytkownik może się zalogować, wydrukować wynik, wyświetlić listę plików.
Po wciśnięciu przycisku OK okno się zamyka i następuje powrót do okna głównego.

Jakie działanie wykonuje system ?
Przykład:
Cyklicznie (codziennie) wysyłana jest poczta do wskazanych użytkowników.

* dane wejściowe → działanie → dane wyjściowe
* ogólne – lista w języku naturalnym
* szczegółowa specyfikacja – dokładna lista, UML, grafy działania (zrobimy w późniejszej fazie projektowania)

Dobra dokumentacja wymagań nie powinna nadmiernie ograniczać projektu aplikacji, to znaczy narzucać konkretnego rozwiązania architektonicznego. Analityk powinien w taki sposób opisywać system, by prezentować dostępne funkcje i możliwości aplikacji bez zbędnego wnikania w szczegóły techniczne.

Podczas opisywania wymagań funkcjonalnych (zarówno listy wymagań, jak i szczegółowej specyfikacji), należy posługiwać się prostymi, jednoznacznymi i zrozumiałymi stwierdzeniami

 **Wymagania niefunkcjonalne**
 Ograniczenia w jakich system ma pracować, standardy jakie spełnia, itp.

Jaki ten system powinien być ?
* Co jest interfejsem ? (strona www, GUI, konsola, inne aplikacje)
* Czy jest przeznaczona dla pojedynczego użytkownika?
* Na jakim systemie operacyjnym działa?
* Jakie ma wymagania (dodatkowe oprogramowanie, dodatkowy sprzęt)?
    * wydajność
    * skalowarność
    * otwartość, możliwość rozbudowy
    * odporność na awarie
    * bezpieczeństwo


## 3) Podział obowiązków w projekcie:
- `Zamawiający` - odpowiada za zgodność systemu ze środowiskiem (w którym pracuje).
Jego zadaniem Jest zapewnienie realizacji przez system celów biznesowych.
Zainteresowany jest dobrym zdefiniowaniem i wspomaganiem usług dostarczanych przez zarządzane przez niego środowisko. Modele opisu środowiska powinny umożliwiać zamawiającemu możliwość oceny dostosowania systemu do potrzeb środowiska.

- `Analityk biznesowy` - prowadzi dialog pomiędzy światem biznesu i Informatyki.
Zajmuje się tworzeniem opisu środowiska poprzez formułowanie odpowiednich modeli.
Często projektuje również (nowe) rozwiązania dla funkcjonującego środowiska.
Tworzy modele umożliwiające opis struktury środowiska oraz jego dynamiki (np. słownika pojęć)

- `Użytkownik` - będzie korzystał z systemu i jest zainteresowany zgodnością systemu ze środowiskiem oraz wygodą posługiwania się systemem.
Pełni podstawową funkcję w zakresie sprawdzania realizacji wymagań.
Modele czytane przez użytkownika powinny dobrze opisywać funkcjonalność oraz struktury danych występujących w systemie.

- `Analityk wymagań` - tworzy wymagania dla systemu oprogramowania.
Jego zadaniem jest zapewnienie zgodności wymagań ze środowiskiem oraz wygody użytkowania systemu. Potrzebuje modelu opisującego funkcjonalność systemu - model ten powinien dostarczać podziału na pojedyncze jednostki funkcjonalności.
(specjalizacje: projektanci interfejsu użytkownika).

- `Architekt` - Jest odpowiedzialny za zaprojektowanie całości systemu. Jest odpowiedzialny realizację systemu tzw.,nadzór architektoniczny" oraz dostosowanie architektury do aktualnych warunków realizacji systemu. Tworzone przez niego modele powinny zapewniać Jednoznaczne powiązanie z wymaganiami i jednocześnie zapewniać możliwość rozbudowy.

- `Projektant` - Wykonuje szczegółowy projekt podsystemów zgodnie z wytycznymi architekta.
Podstawą działań projektanta Jest tzw. „model architektoniczny" systemu.
Projektant opisuje sposób realizacji zdefiniowanych [tam] podsystemów.
Przedstawia strukturę, dynamikę oraz interakcje zachodzące w systemie oraz specyfikuje algorytmy. Tworzone przez niego modele są szczegółowym opisem poszczególnych składników kodu.

- `Programista` - Wykonuje system, pisząc kod odpowiednich podsystemów.
Zadaniem programisty jest zrealizowanie procedur i funkcji zadanych w projekcie.
W związku z tym zostaje zwolniony z definiowania struktury i specyfikowania algorytmów. (Często: projektant-programista)
Ważne jest rozdzielenie czynności projektowych od programistycznych!

- `Recezent` - Pełni rolę sprawdzającego jakość modeli tworzonych przez pozostałe role.
Inspekcja modeli przez niezależną osobę jest bardzo istotnym elementem zapewnienia jakości systemu oprogramowania.

- `Tester` - Sprawdza jakość kodu. Często poprzez porównanie działającego systemu z jego modelem na poziomie wymagań. W celu sprawdzenia wymagań tester projektuje i przeprowadza testy akceptacyjne systemu.

## 4) FURPS
Zbieranie wymagań jest pierwszym krokiem do właściwego testowania. Czynność ta jest wspierana przez metodę klasyfikacji FURPS. Angielski akronim rozszyfrowujemy następująco:

* `Functionality` funkcjonalność w rozumieniu zestawu funkcji uwzględniająca również bezpieczeństwo, możliwości systemu
* `Usability` użyteczność jako zestaw wizualnych aspektów oprogramowania, estetyka, dokumentacja
* `Reliability` niezawodność, będąca mierzona np. częstością występowania błędów
* `Performance` wydajność aplikacji określana również jako czas odpowiedzi lub użycie zasobów
* `Supportability` przenosność, rozszerzalność, nie dająca się łatwo przetłumaczyć “wspieralność” uwzględniająca zdolność aplikacji do instalacji na różnych platformach, łatwość testowania itd.

## 5) Słownik danych
Nie da się modelować wymagań użytkownika bez zdefiniowanych danych występujących w systemie. Słownik danych (DD) stanowi składnicę wszystkich pojęć zdefiniowanych w systemie (projekcie). Jest zorganizowaną listą elementów systemu, zawierającą definicję tych elementów.
W skład słownika danych wchodzą opisy:
* Złożonych agregatów pakietów danych, składających się z komponentów (np. pół rekordów) – używanych przez przepływy danych (data flows).
* Złożonych agregatów danych w składach danych.
* Szczegółowych relacji pomiędzy obiektami diagramów ERD (encji-relacji).

Często używa się następujqcych symboli formalizmu notacji dla DD:
* `=` - składa się z (definicja kompozycji)
* `+` - złączenienie
* `()` - opcia
* `{}` - iteracja
* `[]` - wybranie jednej z kilku możliwych
* `**` - komentarz między znakami `*`
* `@` - identyfikator (pole klucz)
* `|` - oddziela alternatywne wybory w `[]`
```
Osoba = tytuł + imię + (drugie_imię) + nazwisko Tytuł = [ Pan | Pani | Mgr. | Dr | dr hab. | Profesor ] imię = { dowolny_znak }
```

```
drugie_imię = { dowolny_znak }
```

```
dowolny_znak = [A-Z | a-z | 0-9 ]
```