# DIAGRAMY

## 1) Diagram przejść przez stany STD
Diagramy STD opisują zachowanie systemu w czasie. Mogą również służyć do reprezentacji sposobu realizacji funkcji systemu.
Składają się z podstawowych składników:
* Zdarzenie (event) – informuje o tym, co wydarzyło się w danej chwili, ma zerowy czas trwania
* Stan (state) – okres czasu pomiędzy zdarzeniami w systemie. System w różnych stanach reaguje w sposób
jakościowo różny na zachodzące zdarzenia.
* Przejście (transition) – zmiana stanu wywołana przez zdarzenie, może być uwarunkowana spełnieniem dodatkowych
kryteriów.
* Akcja (action) – czynność wykonywana w momencie zajścia zdarzenia.
* Operacja (activity) – związana z konkretnym stanem, oznaczająca czynność ciągłą, wykonywaną podczas trwania stanu.
Operacja może zostać przerwana przez zdarzenia, które powoduje przejście do nowego stanu.

## 2) Diagram przypadków użycia

Diagram przypadków użycia należy do grupy diagramów zachowań.
Pokazuje on system z punktu widzenia użytkownika. Obrazuje, co robi system, a nie jak to robi. 
Najczęściej wraz z diagramami przypadków użycia konieczne jest dostarczenie dokumentacji.

Składa się z:
* `przypadek użycia` - diagram przypadków użycia należy do grupy diagramów zachowań.
Pokazuje on system z punktu widzenia użytkownika. Obrazuje, co robi system, a nie jak to robi. Najczęściej wraz z diagramami przypadków użycia konieczne jest dostarczenie dokumentacji.
![przypadek użycia](./img/1.png)

* `Aktor` - Ma unikalną nazwę. Aktorzy mogą być:
    * Ludźmi wchodzącymi w interakcję z systemem,
    * Systemami zewnętrznymi,
    * Częściami systemu, które mają wpływ na funkcjonowanie systemu, ale same przez ten system nie mogą być zmieniane.

![aktor](./img/2.png)

* `Interakcja` - Obrazuje interakcję pomiędzy przypadkiem użycia a aktorem. Odcinek łączący. (Symbol linii)

* `Blok ponownego użycia` - Obrazuje fragment systemu, który jest używany przez kilka przypadków użycia.
![ponowne użycie](./img/3.png)

* `Relacja include lub extended` -  Pokazuje związek zachodzący między dwoma przypadkami użycia lub przypadkiem użycia a blokiem ponownego użycia.
![relacja](./img/4.png)

* `Nazwa systemu wraz z otoczeniem systemu` - Obrazuje granicę pomiędzy systemem a jego otoczeniem.
![otoczenie](./img/5.png)
  

## 3) Diagram przepływy danych DFD

Diagramy przepływu danych (DFD) wykorzystuje się do modelowania pod kątem przekazywania danych między procesami i obiektami w procesie. DFD służy więc do prezentowania w jaki sposób dane są przetwarzane w systemie. DFD znajdują równie zastosowanie w modelowaniu procesów gospodarczych organizacji oraz w planowaniu gospodarczym i strategicznym.

Tworzenie DFD opiera się na czterech kategoriach pojęciowych:
* Procesów/funkcji – realizujących określone cele
* Przepływów – ilustrujących kierunek przesyłu danych w systemie
* Składnic/magazynów danych – będących argumentami dla procesów i funkcji
* Obiektów zewnętrznych/terminatorów – stanowiących odbiorców bądź źródła danych.

Dwie najczęściej stosowane notacje graficzne służące do opisu DFD: Yourdona-DeMarco i Ganea-Sarsona (np. w MS Visio):
![](./img/6.png)

## 4) Diagram wdrożenia/rozlokowania

Diagramów wdrożenia używa się, by opisać fizyczne aspekty struktury systemy. Główne cele używania diagramów wdrożenia:
* Wizualizacja fizycznej topologii systemu
* Opis komponentów hardwareowych, na których wdrażane są komponenty softwarowe.
![](./img/7.png)

## 5) Diagram komponentów

Diagram komponentów pozwala na modelowanie elementów oprogramowania i związków miedzy nimi. Typowe są dla niego interfejsy, które łączą komponenty.

![](./img/8.png)

## 6) Diagram maszyny stanowej

Maszyna stanowa – zachowanie określające sekwencje stanów (bliźniacze do STD), przez które przechodzi obiekt bądź reakcja w odpowiedzi na zdarzenie.

![](./img/9.png)

## 7) Diagram sekwencji
Diagram sekwencji prezentuje komunikowanie się obiektów w czasie. Składa się z obiektów, komunikatów i czasu.
![](./img/10.png)

## 8) Diagram klas
Diagram klas przedstawia klasy występujące w systemie i statyczne relacje pomiędzy nimi wraz z ograniczeniami. Jest podstawowym diagramem perspektywy logicznej systemu.

![](./img/11.png)
![](./img/12.png)

Z diagramem klas związany jest także **diagram obiektów**:
![](./img/13.png)
Diagram obiektów to wystąpienie diagramu klas, odwzorowujące strukturę systemu w wybranym momencie jego działania.


## 9) Diagram czynności
Diagram czynności przedstawia sekwencje i (lub) współbieżne przepływy sterowania oraz dane pomiędzy uporządkowanymi ciągami czynności, akcji i obiektów.
Stosuje się je w modelowaniu:
* Procesów
* Systemów oraz podsystemów
* Scenariuszy przypadków użycia
* Operacji
* Algorytmów

![](./img/14.png)
![](./img/15.png)

## 10) Diagram UML
Język modelowania UML to zestandaryzowany, formalny język modelowania składający się ze zintegrowanych zbiorów diagramów. Oparty jest na licencji public domain, obecnie rozwijany przez organizację OMG (Object Management Group).
UML stosowany jest do modelowania dziedziny problemu:
* Tworzenie specyfikacji,
* Wizualizacja
* Projektowanie architektury
* Wizualizacja procesów
* Tworzenie diagramów klas i obiektów
* Tworzenie dokumentacji.

UML jest językiem modelowania, a nie programowania. Ułatwia projektowanie systemu z wykorzystaniem metodyki obiektowej. Pozwala obrazować, specyfikować, tworzyć i dokumentować elementy systemu. Nie jest narzędziem ani metodyką.
Posiada dwie składowe: notację elementów oraz ich semantykę (metamodel).

**Notacja:**
 * Elementy graficzne
 * Składnia języka modelowania
 
 * Sposób zapisu modeli elementów

**Metamodel:**
 * Definicje składni i semantyki wprowadzonego modelu
 * Ustala elementy składni diagramów otoczenia typologiczne