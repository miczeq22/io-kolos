# Wzorce Projektowe

**Wzorzec projektowy** (ang. design pattern) – w inżynierii oprogramowania, uniwersalne, sprawdzone w praktyce rozwiązanie często pojawiających się, powtarzalnych problemów projektowych. Pokazuje powiązania i zależności pomiędzy klasami oraz obiektami i ułatwia tworzenie, modyfikację oraz pielęgnację kodu źródłowego. Jest opisem rozwiązania, a nie jego implementacją. Wzorce projektowe stosowane są w projektach wykorzystujących programowanie obiektowe.

Wszystkie wzorce możemy podzielić na następujące rodziny:
- `Kreacyjne (konstrukcyjne)` - opisujące proces tworzenia nowych obiektów; ich zadaniem jest tworzenie, inicjalizacja oraz konfiguracja obiektów, klas oraz innych typów danych.
- `Strukturalne` - opisujące struktury powiązanych ze sobą obiektów.
- `Czynnościowe` - opisujące zachowanie i odpowiedzialność współpracujących ze sobą obiektów.

### Singleton

Singleton jest jednym z najprostszych wzorców projektowych. Jego celem jest ograniczenie możliwości tworzenia obiektów danej klasy do jednej instancji oraz zapewnienie globalnego dostępu do stworzonego obiektu – jest to obiektowa alternatywa dla zmiennych globalnych. Singleton implementuje się poprzez stworzenie klasy, która posiada statyczną metodę getInstance(). Metoda ta sprawdza, czy istnieje już instancja tej klasy, jeżeli nie – tworzy ją i przechowuje jej referencję w prywatnym polu. Aby uniemożliwić tworzenie dodatkowych instancji, konstruktor klasy deklaruje się jako prywatny lub chroniony.

##### Zalety:

- Pobieranie instancji klasy jest niewidoczne dla użytkownika. Nie musi on wiedzieć, czy w chwili wywołania metody instancja istnieje czy dopiero jest tworzona.
- Tworzenie nowej instancji zachodzi dopiero przy pierwszej próbie użycia.
- Klasa zaimplementowana z użyciem wzorca singleton może samodzielnie kontrolować liczbę swoich instancji istniejących w aplikacji.

##### Wady:

- Brak elastyczności, ponieważ już na poziomie kodu, na „sztywno” określana jest liczba instancji klasy.
- Utrudnia testowanie i usuwanie błędów w aplikacji.

### Fabryka abstrakcyjna

Fabryka abstrakcyjna jest wzorcem projektowym, którego zadaniem jest określenie interfejsu do tworzenia różnych obiektów należących do tego samego typu (rodziny). Interfejs ten definiuje grupę metod, za pomocą których tworzone są obiekty. Najczęściej fabrykę abstrakcyjną buduje się w postaci interfejsu. Po stronie klienta tworzone są konkretne implementacje fabryki. Konkretne obiekty tworzone są poprzez wywołanie metod interfejsu. Fabryka pozwala na tworzenie zestawów obiektów dopasowanych do konkretnych zastosowań (np. różnych funkcjonalności, platform, itp.). Każda z konkretnych fabryk realizuje odmienny zestaw klas, ale zawsze posiadają one pewien zdefiniowany zespół interfejsów.

##### Zalety:

- Odseparowanie klas konkretnych – klienci nie wiedzą jakich typów konkretnych używają, posługują się interfejsami abstrakcyjnymi.
- Łatwa wymiana rodziny produktów.
- Spójność produktów – w sytuacji, gdy pożądane jest, aby klasy produkty były z określonej rodziny, fabryka bardzo dobrze to zapewnia.

##### Wady:

- Trudność w dołączaniu nowego produktu do rodzin produktów, spowodowana koniecznością rozszerzania interfejsów fabryki.

### Metoda wytwórcza

Wzorzec metody wytwórczej dostarcza abstrakcji do tworzenia obiektów nieokreślonych, ale powiązanych typów. Umożliwia także dziedziczącym klasom decydowanie jakiego typu ma to być obiekt. Wzorzec składa się z dwóch ról: produktu Product definiującego typ zasobów oraz kreatora Creator definiującego sposób ich tworzenia. Wszystkie typy produktów (ConreteProduct1, ConreteProduct2 itp.) muszą implementować interfejs Product. Z kolei ConcreteCrator dostarcza mechanizm umożliwiający stworzenie obiektu produktu danego typu.

##### Zalety:

- Niezależność od konkretnych implementacji zasobów oraz procesu ich tworzenia.
- Wzorzec hermetyzuje proces tworzenia obiektów, zamykając go za ściśle zdefiniowanym interfejsem.
- Spójność produktów – w sytuacji, gdy pożądane jest, aby klasy produkty były z określonej rodziny.


### Budowniczy

Budowniczy jest wzorcem, gdzie proces tworzenia obiektu podzielony jest na kilka mniejszych etapów, a każdy z nich może być implementowany na wiele sposobów. Dzięki takiemu rozwiązaniu możliwe jest tworzenie różnych reprezentacji obiektów w tym samym procesie konstrukcyjnym. Standardowo wzorzec składa się z dwóch podstawowych elementów. Pierwszy z nich oznaczony jest jako Builder – jego celem jest dostarczenie interfejsu do tworzenia obiektów nazywanych produktami (product). Drugim elementem jest obiekt oznaczony jako ConcreteBuilder, a jego celem jest tworzenie konkretnych reprezentacji produktów przy pomocy zaimplementowanego interfejsu Builder. W ConcreteBuilder zawarte są procedury odpowiedzialne za konstrukcje i inicjalizację obiektu. Strukturę wzorca uzupełnia obiekt Director (zwany także czasem kierownikiem, nadzorcą – tak jak na budowie ;)), który zleca konstrukcję produktów poprzez obiekt Builder dbając o to, aby proces budowy przebiegał w odpowiedniej kolejności.

##### Zalety:

- Duża możliwość zróżnicowania wewnętrznych struktur klas.
- Duża skalowalność (dodawanie nowych reprezentacji obiektów jest uproszczone).
- Większa możliwość kontrolowania tego, w jaki sposób tworzony jest obiekt (proces konstrukcyjny jest niezależny od elementów, z których składa się tworzony obiekt.

##### Wady:

- Duża liczba obiektów reprezentujących konkretne produkty.
- Nieumiejętne używanie wzorca może spowodować nieczytelność kodu (jeden produkt może być tworzony przez zbyt wielu budowniczych).


### Adapter

Wzorzec adapter (znany także pod nazwą wrapper) służy do przystosowania interfejsów obiektowych, tak aby możliwa była współpraca obiektów o niezgodnych interfejsach. Szczególnie przydaje się przypadku wykorzystania gotowych bibliotek o interfejsach niezgodnych ze stosowanymi w aplikacji. W świecie rzeczywistym adapter to przejściówka, np. przejściówka do wtyczki gniazdka angielskiego na polskie. Struktura wzorca składa się z elementów takich jak: Target, Adaptee, Adapter oraz Client. Target jest abstrakcją (zazwyczaj interfejsem), jakiej oczekuje klient. Elementem dostarczającym żądanej przez klienta funkcjonalności jest Adaptee (np. zewnętrzną biblioteką). Rolą adaptera, który implementuje interfejs Target, jest „przetłumaczenie” wywołania metod należących do interfejsu Target poprzez wykonanie innych, specyficznych metod z klasy Adaptee.


### Strategia

Strategia jest wzorcem projektowym, który definiuje rodzinę wymiennych algorytmów i kapsułkuje je w postaci klas. Dzięki temu umożliwia wymienne stosowanie każdego z nich w trakcie działania programu. We wzorcu tym definiujemy wspólny interfejs dla wszystkich strategii. Następnie, w poszczególnych klasach implementujemy metody z konkretnymi już algorytmami. Za pomocą klasy Context możemy łatwo zmieniać używaną strategię w trakcie działania aplikacji.

##### Zalety:

- Eliminacja instrukcji warunkowych – kod jest bardziej przejrzysty.
- Umożliwia wybór implementacji – algorytmy mogą rozwiązywać ten sam problem, lecz różnić się uzyskiwanymi korzyściami.
- Łatwość dołączania kolejnych strategii.
- Łatwiejsze testowanie programu – można debugować każdą strategię z osobna.

##### Wady:

- Dodatkowy koszt komunikacji między klientem, a strategią (wywołania metod, przekazywanie danych).
- „Rozmycie” kodu na kilka klas.


### Obserwator

Głównym obszarem wykorzystania wzorca Obserwator jest stworzenie relacji typu jeden-do-wielu łączącej grupę obiektów. Dzięki zastosowaniu wzorca zmiana stanu (czyli zmiana aktualnych wartości pól) obiektu obserwowanego umożliwi automatyczne powiadomienie o niej wszystkich innych dołączanych elementów (obserwatorów). Interfejs Subject definiuje operacje attach() i detach() pozwalające odpowiednio na dołączanie i odłączanie obserwatorów. Ponadto zdefiniowana jest też operacja notify(), służąca do powiadamiania wszystkich zarejestrowanych obserwatorów o zmianie stanu obiekt obserwowanego poprzez wywołanie w pętli metody update(), która jest zadeklarowana w interfejsie Observer. Operacja ta jest implementowana w klasie realizującej ten interfejs i służy do powiadamiania konkretnego obserwatora o zmianie stanu obiektu obserwowanego.

##### Zalety:

- Luźna zależność między obiektem obserwującym i obserwowanym. Ponieważ nie wiedzą one wiele o sobie nawzajem, mogą być niezależnie rozszerzane i rozbudowywane bez wpływu na drugą stronę.
- Relacja między obiektem obserwowanym a obserwatorem tworzona jest podczas wykonywania programu i może być dynamicznie zmieniana.
- Możliwość zablokowania klientowi drogi do bezpośredniego korzystania ze złożonego systemu, jeśli jest to konieczne.

##### Wady:

- Obserwatorzy nie znają innych obserwatorów, co w pewnych sytuacjach może wywołać trudne do znalezienia skutki uboczne.