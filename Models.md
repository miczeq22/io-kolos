## 1) Model buduj i poprawiaj
Jest to model cyklu życia oprogramowania stosowany podczas produkcji na własny użytek, dla niewielkich problemów.

![](./img/16.png)

## 2) Model V
Jest to zmodyfikowany model kaskadowy, podkreślający wagę weryfikacji i walidacji systemu.

![](./img/17.png)
Każdy etap prowadzi do zwiększenia szczegółowości definicji aż do osiągnięcia dolnego punktu oznaczającego wytworzenie kodu modułów. Kolejne kroki to kroki integracyjne umieszczone na drugim ramieniu.

## 3) Model spiralny
![](./img/18.png)
Model spiralny został zaproponowany w 1988 roku jako ogólna koncepcja iteracyjnego rozwoju oprogramowania. Każde okrążenie po spirali reprezentuje planowanie i wytworzenie pewnego elementu produktu.
* Planowanie – ustalenie celów przygotowania elementu lub produkcji kolejnej wersji systemu, identyfikowanie alternatyw i ograniczeń.
* Analiza ryzyka – ocena alternatyw i oszacowanie ryzyka, ewentualnie jego minimalizacja (np. budowa prototypu).
* Konstrukcja – wytworzenie kolejnego przybliżenia produktu.
* Atestowanie – Walidacja przez klienta, jeśli nie jest pozytywna: rozpoczyna się kolejny proces.

Zalety: 
* Możliwość zmian kierunku rozwoju systemu pomiędzy „obrotami”
* Ścisła współpraca z klientem
* Uświadomienie konieczności zarządzania ryzykiem

Wady: 
* Przydatny dla systemów, które mogą być wdrażane przy okrojonej funkcjonalności i obniżonej jakości
* Osiągnięcie wersji finalnej może wymagać dużo czasu

## 4) Model kaskadowy
![](./img/19.png)
Używamy go, gdy wymagania są dobrze zdefiniowane lub projekt jest podobny do realizowanych wcześniej. Model kaskadowy jest liniowy, sekwencyjny, stanowi punkt wyjścia do budowy innych modeli.

Zalety:
* Łatwość zarządzania
* Ułatwia planowanie, monitorowanie przedsięwzięć
* Wymaga dobrze sprecyzowanych wymagań

Wady:
* Brak weryfikacji, walidacji, elastyczności
* Narzuca ścisłą kolejność wykonywania prac
* Powoduje wysokie koszty błędów popełnionych we wcześniejszych fazach
* Rzadki kontakt z klientem

## 5) Model Kruchtena 4 + 1
Jest jednym z praktycznych sposobów „rozbijania” diagramów UML kolejnych modeli.
![](./img/20.png)